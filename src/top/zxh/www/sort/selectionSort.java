package top.zxh.www.sort;

public class selectionSort {
    public static int[] selection(int[] array) {
//        if (array.length == 0)
//            return array;
//        for (int i = 0; i < array.length; i++) {
//            int minIndex = i;
//            for (int j = i; j < array.length; j++) {
//                if (array[j] < array[minIndex]) //找到最小的数
//                    minIndex = j; //将最小数的索引保存
//            }
//            int temp = array[minIndex];
//            array[minIndex] = array[i];
//            array[i] = temp;
//        }


        for (int i =0;i<array.length;i++){
            int min=i;
            for (int j=i;j<array.length;j++){
                if(array[j]<array[min])
                    min=j;
            }
            int temp ;
            temp =array[i];
            array[i]=array[min];
            array[min]=temp;
        }

        return array;
    }

    public static void main(String[] args) {
        int a []={9,8,7,6,5,4,3,2,1};
        System.out.print("原数据：");
        for (int x:a) {
            System.out.print(x+"\t");
        }
        System.out.println();


        System.out.print("排序后：");
        for (int x:selection(a)) {
            System.out.print(x+"\t");
        }

    }
}
