package top.zxh.www.sort;

public class insertionSort {

    static  int [] insertion(int [] array ){


//        int current;
//        for(int i=0;i<arr.length-1;i++){
//            current =arr[i+1];
//            int contrast =i;
//            while (contrast>=0&&current<arr[contrast]){
//                arr[contrast+1]=arr[contrast];
//                contrast--;
//            }
//            arr[i+1]=current;
//        }
//        return arr;

        int current;
        for (int i = 0; i < array.length - 1; i++) {
            current = array[i + 1];
            int preIndex = i;

            for( ;preIndex>=0;preIndex--){
                if(current < array[preIndex]){
                    array[preIndex + 1] = array[preIndex];
                }

            }

//            while (preIndex >= 0 && current < array[preIndex]) {
//                array[preIndex + 1] = array[preIndex];
//                preIndex--;
//            }
            array[preIndex + 1] = current;
        }
        return array;
    }

    public static void main(String[] args) {
        int a []={9,8,7,6,5,4,3,2,1};
        System.out.print("原数据：");
        for (int x:a) {
            System.out.print(x+"\t");
        }
        System.out.println();


        System.out.print("排序后：");
        for (int x:insertion(a)) {
            System.out.print(x+"\t");
        }

    }
}
