package top.zxh.www.sort;

public class bubbleSort {


    static  public int [] bubble(int arr []){

        for(int i=0;i<arr.length-1;i++)
            for (int j=0;j<arr.length-1-i;j++)
            if(arr[j]>arr[j+1])
            {   int temp ;
                temp =arr[j];
                arr[j]=arr[j+1];
                arr[j+1]= temp;

            }

        return  arr;
    }


    public static void main(String[] args) {

        int a []={9,8,7,6,5,4,3,2,1};
        System.out.print("原数据：");
        for (int x:a) {
            System.out.print(x+"\t");
        }
        System.out.println();


        System.out.print("排序后：");
        for (int x:bubble(a)) {
            System.out.print(x+"\t");
        }

    }

}
